/**
 * Created by Александр on 13.12.2015.
 */
module.exports = function (express, botApi) {
    var router = express.Router();

    return {
        endPoint: '/aneks',
        router: router
    };
};